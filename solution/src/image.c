#include <stdio.h>
#include <stdlib.h>

#include "image.h"

void imageFree(struct image *image) {
    if(!image) return;
    image->width = 0;
    image->height = 0;
    free(image->data);
    image->data = NULL;

}

void imageInit(struct image *image, uint64_t width, uint64_t height){
    (*image).width = width;
    (*image).height = height;
    (*image).data = calloc(width * height, sizeof(struct pixel));
    if (image->data == 0) {
        fprintf(stderr, "%s", "Failed to allocate memory for the image data\n");
    }
}

struct pixel imageGet(struct image const* image, uint64_t x, uint64_t y) {
    return image->data[image->width * y + x];
}

void imageSet(struct image *image, uint64_t x, uint64_t y, struct pixel pixel) {
    image->data[image->width * y + x] = pixel;
}


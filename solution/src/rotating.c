#include "rotating.h"

struct image rotating(struct image* img ){
// Create a new image structure to hold the rotated image
    struct image rotated;
    imageInit(&rotated, img->height, img->width);

    // Rotate the image 90 degrees counterclockwise
    for (uint64_t y = 0; y < rotated.height; y++) {
        for (uint64_t x = 0; x < rotated.width; x++) {
            rotated.data[y * rotated.width + x] = img->data[(img->height - x) * img->width + y - img->width];
        }
    }

    return rotated;
}

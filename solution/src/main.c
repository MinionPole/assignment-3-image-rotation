#include "image.h"
#include "bmp_format.h"
#include "rotating.h"
#include <stdio.h>


int main( int argc, char** argv ) {

    if (argc != 3) {
        printf("Usage: %s input.bmp output.bmp\n", argv[0]);
        return 1;
    }
    /* открытие файлов*/
    FILE *in_file = fopen(argv[1], "rb");
    if (!in_file) {
        printf("Error opening input file\n");
        return 1;
    }
    FILE *out_file = fopen(argv[2], "wb");
    if (!out_file) {
        printf("Error opening output file\n");
        return 1;
    }
    /* открытие файлов*/

    //чтение картинки
    struct image img;
    if(fromBmp(in_file, &img)){
        fclose(in_file);
        fclose(out_file);
        imageFree(&img);
        fprintf(stderr, "Failed to read bmp image\n");
    }
    if (!img.data) {
        fprintf(stderr, "Failed to read input image\n");
        return 1;
    }



    // Rotating the image
    struct image rotated = rotating(&img);
    if (toBmp(out_file, &rotated) != WRITE_OK) {
        fclose(in_file);
        fclose(out_file);
        imageFree(&rotated);
        imageFree(&img);
        fprintf(stderr, "Failed to write output image\n");
        return 1;
    }
    
    // Free up the memory used by the image
    imageFree(&img);
    imageFree(&rotated);
    // Close the files
    fclose(in_file);
    fclose(out_file);

    return 0;
}

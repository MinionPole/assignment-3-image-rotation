#include "bmp_format.h"
#include "image.h"
#define BMP_TYPE 0x4d42
#define BMP_RESERVED 0
#define BMP_PLANES 1
#define BMP_COMPRESSION 0
#define BMP_BITS 24
#define BMP_IMAGE_SIZE 0
#define BMP_X_PELS_PER_METER 0
#define BMP_Y_PELS_PER_METER 0
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0
#define BI_SIZE 14

uint64_t calcPadding(uint64_t width) {
    return 4 - sizeof(struct pixel) * width % 4;
}

enum reading_end fromBmp(FILE* in, struct image* img) {
    // Read the BMP header
    struct bmp_header header;
    if(!fread(&header, sizeof(header), 1, in)){
        return READ_INVALID_HEADER;
    };

    // Check that this is a BMP file
    if (header.bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    const size_t padding = calcPadding(header.biWidth);

    // Read the image data
    imageInit(img, header.biWidth, header.biHeight) ;
    for(uint64_t i = 0; i < img->height; i++){
        if (fread(img->data + img->width * i, sizeof(struct pixel), img->width, in) != img->width) {
            imageFree(img);
            return READ_INVALID_BITS;
        }
        if (fseek(in, (int64_t)padding, SEEK_CUR)) {
            imageFree(img);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

// Writes an 'image' struct to a BMP image file
enum writing_end toBmp(FILE* out, const struct image* img) {
    // Write the BMP header
    struct bmp_header header = {0};
    header.bfType = BMP_TYPE;
    header.bfileSize = sizeof(header) + img->width * img->height * sizeof(struct pixel) + calcPadding(img->width);
    header.bfReserved = BMP_RESERVED;
    header.bOffBits = sizeof(header);
    header.biSize = sizeof(header) - BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes =  BMP_PLANES;
    header.biBitCount = BMP_BITS;
    header.biCompression = BMP_COMPRESSION;
    header.biSizeImage = BMP_IMAGE_SIZE;
    header.biXPelsPerMeter = BMP_X_PELS_PER_METER;
    header.biYPelsPerMeter = BMP_Y_PELS_PER_METER;
    header.biClrUsed = BMP_CLR_USED;
    header.biClrImportant = BMP_CLR_IMPORTANT;
    if (fwrite(&header, sizeof header, 1, out) != 1) {
        return WRITE_ERROR;
    }
    // Write the image data
    for (size_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fseek(out, (int64_t)calcPadding(img->width), SEEK_CUR);
    }

    return WRITE_OK;
}

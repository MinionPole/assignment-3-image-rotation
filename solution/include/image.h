#pragma once

#include <stdint.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void imageInit(struct image *image, uint64_t width, uint64_t height);

void imageFree(struct image *image);

struct pixel imageGet(struct image const* image, uint64_t x, uint64_t y);

void imageSet(struct image *image, uint64_t x, uint64_t y, struct pixel pixel);
